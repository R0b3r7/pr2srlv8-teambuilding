//#pragma once

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

typedef struct clan {

	unsigned int id;
	char ime[31];
	char prezime[31];
	char adresa[51];
	char brojMobitela[14];

}CLAN;

void fProvjeraDatoteke(char*, unsigned int*);
void fDodajKorisnika(char*, unsigned int*);
void fProcitajKorisnike(char*, unsigned int*);
void fIzlazakIzPrograma(void);
void fMenu(char*, unsigned int*);

#endif //FUNCTIONS_H
