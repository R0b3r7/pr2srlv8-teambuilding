#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "functions.h"
#include <string.h>

void fProvjeraDatoteke(char* datoteka, unsigned int* pBrojKorisnika) {

	FILE* pDatotekaProvjera = fopen(datoteka, "rb");

	if (pDatotekaProvjera == NULL) {

		perror("Datoteka ne postoji, prilikom prvog pokretanja");

		pDatotekaProvjera = fopen(datoteka, "wb");

		if (pDatotekaProvjera == NULL) {

			perror("Dadoteka se ne mo�e kreirati, prilikom prvog pokretanja");
			exit(EXIT_FAILURE);
		}
		else {

			fwrite(pBrojKorisnika, sizeof(unsigned int), 1, pDatotekaProvjera);
			fclose(pDatotekaProvjera);
			printf("Datoteka kreirana, prilikom prvog pokretanja\n");
		}
	}
	else {

		fread(pBrojKorisnika, sizeof(unsigned int), 1, pDatotekaProvjera);
		fclose(pDatotekaProvjera);
		printf("Datoteka postoji, prilikom prvog pokretanja\n");
	}
}

void fDodajKorisnika(char* datoteka, unsigned int* pBrojKorisnika) {

	FILE* pDatotekaDodajNaKraj = NULL;
	//pDatotekaDodajNaKraj = fopen(clanovi, "ab");
	pDatotekaDodajNaKraj = fopen(datoteka, "rb+");

	if (pDatotekaDodajNaKraj == NULL) {
		perror("Izbornik 1 - Dodavanje novog clana u datoteku");
		return;
		//exit(EXIT_FAILURE);
	}
	else {

		CLAN privremeniClan = { 0 };
		printf("Unesite ime korisnika\n");
		scanf("%30s", privremeniClan.ime);
		printf("Unesite prezime korisnika\n");
		scanf("%30s", privremeniClan.prezime);
		printf("Unesite adresu korisnika\n");
		scanf(" %50[^\n]", privremeniClan.adresa);
		printf("Unesite broj mobitela bez +385\n");
		char privremeniBroj[10] = { '\0' };
		scanf("%9s", privremeniBroj);
		strcpy(privremeniClan.brojMobitela, "+385");
		strcat(privremeniClan.brojMobitela, privremeniBroj);
		privremeniClan.id = (*pBrojKorisnika)++;

		fseek(pDatotekaDodajNaKraj, sizeof(unsigned int) + ((*pBrojKorisnika - 1) * sizeof(CLAN)), SEEK_SET);
		fwrite(&privremeniClan, sizeof(CLAN), 1, pDatotekaDodajNaKraj);
		rewind(pDatotekaDodajNaKraj);
		fwrite(pBrojKorisnika, sizeof(unsigned int), 1, pDatotekaDodajNaKraj);
		fclose(pDatotekaDodajNaKraj);
	}
}

void fProcitajKorisnike(char* datoteka, unsigned int* pBrojKorisnika) {

	FILE* pDatotekaProcitaj = NULL;
	pDatotekaProcitaj = fopen(datoteka, "rb");

	if (pDatotekaProcitaj == NULL) {

		perror("Izbornik 2 - Citanje datoteke");
		return;
		//exit(EXIT_FAILURE);
	}
	else {

		CLAN* sviKorisnici = NULL;

		fread(pBrojKorisnika, sizeof(unsigned int), 1, pDatotekaProcitaj);

		if (*pBrojKorisnika == 0) {

			printf("Nema unesenih clanova!\n");
			fclose(pDatotekaProcitaj);
			return;
		}
		else {

			sviKorisnici = (CLAN*)calloc(*pBrojKorisnika, sizeof(CLAN));

			if (sviKorisnici == NULL) {

				perror("Citanje svih korisnika");
				exit(EXIT_FAILURE);
			}
			else {

				fread(sviKorisnici, sizeof(CLAN), *pBrojKorisnika, pDatotekaProcitaj);
				fclose(pDatotekaProcitaj);

				unsigned int i;

				printf("Unesite ime korisnika\n");
				char privremenoIme[31] = { '\0' };
				scanf("%30s", privremenoIme);
				unsigned int statusPronalaska = 0;
				unsigned int indeksPronalaska = -1;

				for (i = 0; i < *pBrojKorisnika; i++)
				{
					printf("%u\t", (sviKorisnici + i)->id);
					printf("%s ", (sviKorisnici + i)->ime);
					printf("%s ", (sviKorisnici + i)->prezime);
					printf("%s ", (sviKorisnici + i)->adresa);
					printf("%s\n", (sviKorisnici + i)->brojMobitela);

					if (!strcmp((sviKorisnici + i)->ime, privremenoIme)) {
						statusPronalaska = 1;
						indeksPronalaska = i;
					}
				}

				if (statusPronalaska) {
					printf("Korisnik pronadjen\n");
					printf("%u\t", (sviKorisnici + indeksPronalaska)->id);
					printf("%s ", (sviKorisnici + indeksPronalaska)->ime);
					printf("%s ", (sviKorisnici + indeksPronalaska)->prezime);
					printf("%s ", (sviKorisnici + indeksPronalaska)->adresa);
					printf("%s\n", (sviKorisnici + indeksPronalaska)->brojMobitela);
				}
				else {

					printf("Nepostojeci korisnik\n");
				}

				free(sviKorisnici);
			}
		}
	}
}

void fIzlazakIzPrograma(void) {

	printf("Da li ste sigurni kako zelite zavrsiti program?\n");
	char izbor[3] = { '\0' };
	scanf(" %2s", izbor);
	if (!strcmp("da", izbor)) {
		exit(EXIT_FAILURE);
	}

	return;
}

void fMenu(char* datoteka, unsigned int* pBrojKorisnika) {

	unsigned int izbornik = -1;

	while (1) {

		printf("|********************************************\
				\n1. Dodavanje novih clanova u datoteku clanovi.bin\
				\n2. Citanje iz datoteke clanovi.bin\
				\n3. Zavrsetak programa\
				\n*********************************************|\n");

		scanf("%u", &izbornik);

		switch (izbornik) {
		case 1:
			fDodajKorisnika(datoteka, pBrojKorisnika);
			break;
		case 2:
			fProcitajKorisnike(datoteka, pBrojKorisnika);
			break;
		case 3:
			fIzlazakIzPrograma();
			break;
		default:
			printf("Krivo odabrana opcija, pokusajte ponovno\n");
		}
	}
}