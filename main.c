#define _CRT_SECURE_NO_WARNINGS
/*
Napisati C program kojim �e se omogu�iti vo�enje evidencije �lanova jedne videoteke. Program
treba omogu�iti korisniku izbornik unutar kojeg �e korisnik mo�i odabrati neku od radnji. Program
treba kreirati datoteku clanovi.bin, ukoliko datoteka ne postoji na disku (prvi puta kada se pokrene
program). �lanovi videoteke predstavljeni su strukturom CLAN s �lanovima strukture koji su
stringovi: ID, ime, prezime, adresa i broj mobitela. Na po�etku programa ponuditi korisniku
izbornik za odabir �eljene radnje:
1. Dodavanje novih �lanova u datoteku clanovi.bin,
2. �itanje iz datoteke clanovi.bin,
3. Zavr�etak programa.

Ako se �eli unijeti novi korisnik, svaki puta je potrebno odabrati opciju br. 1 i s tipkovnice unijeti
podatke o korisniku, tako�er je potrebno brojati koliko se novih korisnika unijelo, te svakim novim
unosom korisnika potrebno je zapisati korisnika u datoteku. Voditi evidenciju broja unesenih
korisnika. Broj korisnika je potrebno zapisati na po�etku datoteke, a tek poslije cijele vrijednosti je
potrebno zapisati strukturu CLAN.

Ako je potreba za �itanjem korisnika iz datoteke potrebno je odabrati opciju br. 2. Potrebno je prvo
pro�itati cijelu vrijednost koja predstavlja ukupni broj spremljenih korisnika unutar datoteke, te
prona�i korisnika po odre�enom kriteriju pretra�ivanja, mo�e biti po bilo kojem �lanu strukture
CLAN.

Ako je potrebno zavr�iti s programom, potrebno je odabrati opciju br. 3, nakon kojeg �e se
korisnika upitati s porukom "Da li ste sigurni kako zelite zavrsiti program?" popra�eno s opcijom
da/ne koju je potrebno utipkati. Ako je odabrana opcija "da" zatvoriti program, ako je odabrana
opcija "ne" ponuditi ponovni izbor radnji.
*/

#include <stdio.h>
#include "functions.h"



int main(void) {

	char* clanovi = "clanovi.bin";
	unsigned int brojKorisnika = 0;
	
	fProvjeraDatoteke(clanovi, &brojKorisnika);
	fMenu(clanovi, &brojKorisnika);
	
	return 0;
}